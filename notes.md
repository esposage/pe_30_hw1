2.1. Вам потрібно додати нове домашнє завдання в новий репозиторій на Gitlab.
1. Cтворити віддалений репозиторій на Gitlab.com
2. Клонувати репозиторій на свій комп"ютер за допомогою команди git clone
3. Створити потрібні файли для виконання проєкту, зробити проєкт.
4. Для того, щоб відправити готовий проєкт на віддалений репозиторій, потрібно виконати команди: 
- git add . - команда додає файли в зону індексації,відслідковування.
- git commit -a -m " message " - команда робить коміт змін, які будуть відправлені у віддалений репозиторій.
- git push origin main - команда відправляє коміт на віддалений репозиторій.

2.2 Вам потрібно додати існуюче домашнє завдання в новий репозиторій на Gitlab.
1. Cтворити віддалений репозиторій на Gitlab.com
2. В терміналі прописати наступні команди:
- git init — команда зв’язує локальну папку на комп’ютері з системою контролю версій git.
- git remote add origin <link>  — команда зв’язує наш локальний репозиторій, з віддаленим, де <link> - це посилання на власне віддалений репозиторій.
- git add . - команда додає файли в зону індексації,відслідковування.
- git commit -a -m " message " - команда робить коміт змін, які будуть відправлені у віддалений репозиторій.
- git push origin main - команда відправляє коміт на віддалений репозиторій. 